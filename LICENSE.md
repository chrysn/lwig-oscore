# License

See the
[guidelines for contributions](https://github.com/git@gitlab.com:chrysn/lwig-oscore/blob/master/CONTRIBUTING.md).
