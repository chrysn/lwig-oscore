# OSCORE Implementation Guidance

This is the working area for the individual Internet-Draft, "OSCORE Implementation Guidance".

* [Editor's Copy](https://chrysn.gitlab.io/lwig-oscore/)
* [Individual Draft](https://tools.ietf.org/html/draft-amsuess-lwig-oscore)

## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/master/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://gitlab.com/chrysn/lwig-oscore/-/blob/master/CONTRIBUTING.md).
